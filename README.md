# Goldspark IT REST API Framework

An easy-to-use, versioning-enabled REST API framework for PHP and MongoDB.

## Getting started

Please read the instructions for an easy and painless installation.

### Prerequisites

* Apache web server
* mod_rewrite for Apache
* PHP 7.0 (other versions may work, but are not guaranteed to work)
* MongoDB server
* MongoDB PHP extension (>= 1.4.0)

### Installation

Copy the files in the `private_html` folder to the root. Then follow the instructions under Configuration.

### Configuration

Make sure to create the configuration files in the `versioning/v1/config` folder. We recommend you use the .tpl files in the `versioning/v1/config` folder as the base for the configuration files.

The following configuration files have to be created:

* `versioning/v1/config/api.php`
(template: `versioning/v1/config/api.tpl`)
* `versioning/v1/config/db.php`
(template: `versioning/v1/config/db.tpl`)

## Calling functions
The URI format is as follows:
`v[VERSION]/[FUNCTION]/[PARAMETERS]`

Parameters are optional and are separated by forward slashes.

Example:
`v1/books/author/Adams`

## Creating functions

To create a function, simply create a PHP file with the function name in `versioning/v1/functions`. The file has to contain a class with the function name (First letter should be uppercase, other letters should be lowercase). This class should always extend the `ApiFunction` class.

The class should contain a function for each accepted HTTP method. The example class below accepts GET and PUT requests. If an HTTP method is used for which there is no function, the framework will automatically output a *405 Method Not Allowed* error.

### Example
*versioning/v1/functions/example.php*

    <?php
    
    class Example extends ApiFunction {
        function get() {
            $this->output(
	            array(
	                "example" => "The function was called with method GET"
                )
            );
        }
    
        function put() {
            $this->output(
	            array(
	                "example" => "The function was called with method PUT"
                )
            );
        }
    }

The function can now be called using the URI `v1/example`.

## ApiFunction class

All function classes are based on the `ApiFunction` class. This class has several useful functions for use in your API:

* `error($statusCode)`
This function generates an HTTP error based on the specified status code. This will end execution.

* `limitParamaterCount($min = 0, $max = 0)`
This function generates a *400 Bad Request* error when the number of parameters supplied by the client in the URI fall outside of the specified bounds.

* `output($output, $type = "application/json")`
This function sets the `Content-Type` HTTP header and sends the output to the client. If `$type` is `application/json` or simply not specified, the framework will output `$output` as a JSON object. In this case, an array has to be provided. If `$type` is not equal to `application/json`, the framework will simply echo the data in `$output`.

* `status($statusCode)`
This function sets the HTTP status code in the response header.

## Versioning

The framework is built with versioning in mind. If a function is not available in the requested version, the framework will automatically try to load an older version.

Creating a new version is as easy as creating a new folder in the versioning folder named `v[VERSION]`. For example, to create version 2, create the folder `versioning/v2.`

Each versioning folder must contain a `functions` folder. It can optionally contain a `config` folder and an `include` folder. All PHP files in the `config` and `include` folders will automatically be loaded. If a PHP file is not available for the specified version, but is available for an older version, the older version will automatically be loaded.

## Disclaimer/Support
This framework is supplied as-is and without any support.