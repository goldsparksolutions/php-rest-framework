<?php

class Versioning {
    private static $version = 1;
    
    public static function setVersion($version) {
        if (!file_exists(__DIR__ . "/../versioning/v" . $version . "/")) {
            header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found");
            die("404 Not Found");
        }
        
        self::$version = intval($version);
    }
    
    public static function getVersion() {
        return self::$version;
    }
    
    public static function requireOnce($file) {
        for ($version = self::$version; $version > 0; --$version) {
            $filePath = __DIR__ . "/../versioning/v" . $version . "/" . str_replace("..", "", $file);
            
            if (file_exists($filePath)) {
                require_once($filePath);
                return true;
            }
        }
        
        return false;
    }
    
    public static function loadAll($path) {
        $loadedFiles = array();
        
        for ($version = self::$version; $version > 0; --$version) {
            $versionFiles = glob(__DIR__ . "/../versioning/v" . $version . "/" . ltrim(rtrim(str_replace("..", "", $path), '/'), '/') . "/*.php");
            
            foreach ($versionFiles as $versionFile) {
                $baseName = basename($versionFile);
                
                if (in_array(strtolower($baseName), $loadedFiles)) {
                    continue;
                }
                
                $loadedFiles[] = strtolower($baseName);
                
                require_once($versionFile);
            }
        }
        
        return true;
    }
}