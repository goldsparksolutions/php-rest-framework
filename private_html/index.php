<?php
// Load Mongo compatibility library for PHP 7 if Mongo extension is not available
if (!extension_loaded("mongo")) {
    // Check availability of MongoDB extension
    if (!extension_loaded("mongodb")) {
        header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error");
        die("500 Internal Server Error - MongoDB extension not found");
    }
    
    // Check version of MongoDB extension
    if (version_compare(phpversion("mongodb"), "1.4.0") == -1) {
        header($_SERVER['SERVER_PROTOCOL'] . " 500 Internal Server Error");
        die("500 Internal Server Error - MongoDB extension too old  (1.4.0 or higher is required)");
    }
    
    // Register autoloader
    spl_autoload_register(function ($className) {
        $className = str_replace("\\", "/", $className);
        
        $baseFolder = __DIR__ . "/include/mongo/";
        
        $folders = array(
            "",
            "Mongo",
            "MongoDB",
            "MongoDB/Exception",
            "MongoDB/GridFS",
            "MongoDB/GridFS/Exception",
            "MongoDB/Model",
            "MongoDB/Operation",
            "Alcaeus/MongoDbAdapter",
            "Alcaeus/MongoDbAdapter/Helper",
        );
        
        foreach ($folders as $folder) {
            if (empty($folder)) {
                $fileName = $baseFolder . $className . ".php";
            } else {
                $fileName = $baseFolder . $folder . "/" . $className . ".php";
            }
            
            if (file_exists($fileName)) {
                include($fileName);
                
                if (file_exists(dirname($fileName) . "/functions.php")) {
                    include_once(dirname($fileName) . "/functions.php");
                }
                
                return;
            }
        }
    });
}

// Get path and version
$version = $_GET['__version'];
$path = $_GET['__path'];

unset($_GET['__version']);
unset($_GET['__path']);

// Include versioning class
require_once(__DIR__ . "/include/versioning.php");

// Set version
Versioning::setVersion($version);

// Load all config and include files for specified version
Versioning::loadAll("config");
Versioning::loadAll("include");

// Get URI segments
$uriSegments = explode('/', ltrim($path, '/'));

// Get function name
$function = str_replace('\\', "", str_replace(".", "", array_splice($uriSegments, 0, 1)[0]));

// Throw error if function is empty
if (empty($function)) {
    header($_SERVER['SERVER_PROTOCOL'] . " 400 Bad Request");
    die("400 Bad Request");
}

// Assemble function file path
$functionFile = "functions/" . strtolower($function) . ".php";

// Load function file or throw an error if file could not be found
if (!Versioning::requireOnce($functionFile)) {
    header($_SERVER['SERVER_PROTOCOL'] . " 404 Not Found");
    die("404 Not Found");
}

// Load function class
$className = ucfirst($function);

$class = new $className();

// Run function
$class->run($uriSegments);