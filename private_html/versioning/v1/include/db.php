<?php

class Db {
    static private $db = null;
    
    static function get() {
        if (is_null(self::$db)) {
            $user = Config\Db::$user;
            $pwd = Config\Db::$password;
            $host = Config\Db::$host;
            $db = Config\Db::$db;
            
            $client = new MongoClient("mongodb://" . urlencode($user) . ":" . urlencode($pwd) . "@" . $host . "/" . urlencode($db));
            
            self::$db = $client->$db;
        }
        
        return self::$db;
    }
}