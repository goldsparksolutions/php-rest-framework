<?php

class About extends ApiFunction {
    function get() {
        $this->limitParameterCount(0, 0);
        
        $this->output(array(
            "version" => Versioning::getVersion(),
            "copyright" => str_replace("%YEAR%", date("Y"), Config\Api::$copyright)
        ));
    }
}